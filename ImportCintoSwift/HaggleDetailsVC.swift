//
//  HaggleDetailsVC.swift
//  Haggles
//
//  Created by Aditi Manna on 30/07/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

import UIKit

class HaggleDetailsVC: UIViewController,LoginPopUpDelegate {
   // var login:LoginPopUp = LoginPopUp()
    @IBAction func btnJoinHagglerAction(sender: UIButton) {
        self.performSegueWithIdentifier("segueJoinHaggle", sender: self)
//              var strUser : String  =  NSUserDefaults.standardUserDefaults().valueForKey("userId") as! String
//        
//        if(strUser.isEmpty){
//             self.performSegueWithIdentifier("segueLogin", sender: self)
//        }else{
//             self.performSegueWithIdentifier("segueJoinHaggle", sender: self)
//        }
        
       
    }

    @IBAction func btnJoinAction(sender: UIButton) {
        
        
    }
    @IBAction func btnBackAction(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    func SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(version: NSString) -> Bool {
        return UIDevice.currentDevice().systemVersion.compare(version as String,
            options: NSStringCompareOptions.NumericSearch) != NSComparisonResult.OrderedAscending
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let customTabBarVc = SSTabBarController(nibName: "SSTabBarController", bundle: nil)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let welcomeVC = storyboard.instantiateViewControllerWithIdentifier("MenuVC") as! MenuVC
        let WelcomeVcNavigation = UINavigationController(rootViewController:welcomeVC)
        WelcomeVcNavigation.navigationBar.hidden = true
        customTabBarVc.viewControllers = NSArray(object:WelcomeVcNavigation) as [AnyObject]
        self.view.addSubview(customTabBarVc.view)
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO("7.0"))
        {
            let view :UIView = UIView(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, 20))
            // view.backgroundColor = UIColor(red: 71/255, green: 154/255, blue: 65/255, alpha: 1)
            view.backgroundColor = UIColor(patternImage: UIImage(named: "green_patch.png")!)
            
            self.view.addSubview(view)
        }
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func signUpClicked(){
         self.performSegueWithIdentifier("segueLogin", sender: self)
    }
    func loginClicked(){
        //segueLogin
        self.performSegueWithIdentifier("segueLogin", sender: self)
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "segueLogin") {
            let secondViewController = segue.destinationViewController as! LoginVC
            secondViewController.isJoinHaggle="join"
            
        }else{
            
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
