//
//  SignupVC.swift
//  Haggles
//
//  Created by Aditi Manna on 04/08/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

import UIKit

class SignupVC: UIViewController,UITextFieldDelegate{
    //var hud: MBProgressHUD = MBProgressHUD()
    @IBAction func btnBackAction(sender: UIButton) {
        if let navController = self.navigationController {
            navController.popViewControllerAnimated(true)
        }
    }

    @IBOutlet weak var myView: UIView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPass: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBAction func btnSignUpAction(sender: UIButton) {
        self.txtEmail.resignFirstResponder()
         self.txtPass.resignFirstResponder()
         self.txtName.resignFirstResponder()
       var hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        let global = GlobalConstants()
        let rest = RestAPISession()
        rest.signup(self.txtEmail.text, password:self.txtPass.text , uname: self.txtName.text, url: global.kBaseURLSignUp) { (succeeded:Bool, msg:String) -> () in
            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
            if(succeeded){
                self.performSegueWithIdentifier("segueLocation", sender: self)
            }else{
                var refreshalert = UIAlertView(title:"SignUp", message:msg, delegate:nil, cancelButtonTitle: "OK")
                refreshalert.show()
            }
        }
        
    //self.performSegueWithIdentifier("segueLocation", sender: self)
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        if(textField == self.txtName){
            self.txtEmail .becomeFirstResponder()
            self.txtName.resignFirstResponder()
        }else if(textField == self.txtEmail){
            self.txtPass .becomeFirstResponder()
            self.txtEmail.resignFirstResponder()
        }else{
            self.txtPass.resignFirstResponder()
        }
        return true
    }
    @IBAction func btnLoginAction(sender: UIButton) {
       self.performSegueWithIdentifier("segueLogin", sender: self)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // [[UITextField appearance] setTintColor:[UIColor blackColor]];
       // self.txtName.becomeFirstResponder()
        self.myView.backgroundColor = UIColor(patternImage: UIImage(named: "sign-up&forget_blur.png")!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
