//
//  BuyProductVC.swift
//  Haggles
//
//  Created by Aditi Manna on 06/08/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

import UIKit

class BuyProductVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {
    @IBOutlet weak var viewProd: UIView!
    @IBOutlet weak var prodCollectionView: UICollectionView!
    @IBAction func btnUserAction(sender: UIButton) {
    }
    var screenSize: CGRect!
    var screenWidth: CGFloat!
    var screenHeight: CGFloat!
    @IBOutlet weak var layout: UICollectionViewFlowLayout!
    @IBAction func btnMenuAction(sender: UIButton) {
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        screenSize = UIScreen.mainScreen().bounds
        screenWidth = screenSize.width - 32
        screenHeight = self.prodCollectionView.frame.size.height
        // Do any additional setup after loading the view.
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
                return CGSize(width: screenWidth/3, height: 368/3);
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return 9
    }
    
    // The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        let cell = prodCollectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! ProductCell
        
        if(indexPath.row == 0){
            cell.htconstarint.constant = 100;
            cell.widthconstarint.constant = 100;
        }else{
             cell.widthconstarint.constant = 50;
             cell.htconstarint.constant = 50;
        }
        //cell.backgroundColor = UIColor.blackColor()
        // Configure the cell
        return cell
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
