//
//  LoginPopUp.swift
//  Haggles
//
//  Created by Aditi Manna on 18/08/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

import UIKit

 protocol LoginPopUpDelegate{
     func signUpClicked()
     func loginClicked()
}

class LoginPopUp: UIViewController {
     var delegate:LoginPopUpDelegate?
    
    @IBOutlet weak var myView: UIView!
    
    @IBAction func btnLoginAction(sender: UIButton) {
        self.dismissViewControllerAnimated(false, completion: nil)
        delegate?.loginClicked()
    }
    @IBAction func btnSignUpAction(sender: UIButton) {
        self.dismissViewControllerAnimated(false, completion: nil)
        delegate?.signUpClicked()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
        self.myView.layer.cornerRadius = 5;
        self.myView.layer.masksToBounds = true;
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
