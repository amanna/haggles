//
//  RestAPISession.swift
//  Haggles
//
//  Created by Aditi Manna on 19/08/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

import UIKit

class RestAPISession: NSObject {
    let global = GlobalConstants()
    func login(email : String,password:String, url : String, postCompleted : (succeeded: Bool, msg: String) -> ()) {
        var request = NSMutableURLRequest(URL: NSURL(string: url)!)
        var session = NSURLSession.sharedSession()
        request.HTTPMethod = "POST"
         let postParams = "UserLogin[username]=" + email + "&UserLogin[password]=" + password
        var err: NSError?
       request.HTTPBody = postParams.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        
        var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            println("Response: \(response)")
            var strData = NSString(data: data, encoding: NSUTF8StringEncoding)
            println("Body: \(strData)")
            var err: NSError?
            var json: AnyObject? = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil)
            
            var msg = "No message"
            var statusDict : NSDictionary = json?.valueForKey("status") as! NSDictionary
            
            var success = statusDict.valueForKey("status")!.integerValue!
            // self.error_code = NSString(format:"%d",success!) as String
            var errorMsg:String = statusDict.valueForKey("error") as! String
            
            
           
            // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
            if(err != nil) {
                 postCompleted(succeeded: false, msg: "Some Problem occurs!!Please try again later")
            }
            else {
                if(success==1){
                    var dictResult: NSDictionary = json?.valueForKey("result") as! NSDictionary
                    var dictUser: NSDictionary = dictResult.valueForKey("user") as! NSDictionary
                    var email: String = dictUser.valueForKey("email") as! String
                    var uid: String = dictUser.valueForKey("id") as! String
                    var uname: String = dictUser.valueForKey("username") as! String
                    
                    
                    NSUserDefaults.standardUserDefaults().setObject(uid, forKey:"userId")
                    NSUserDefaults.standardUserDefaults().setObject(uname, forKey:"username")
                    NSUserDefaults.standardUserDefaults().setObject(email, forKey:"email")
                    NSUserDefaults.standardUserDefaults().synchronize()
                     postCompleted(succeeded: true, msg: "Log in Successful")
                }else{
                     postCompleted(succeeded: false, msg: "Invalid User Name or Password")
                }
            }
        })
        
        task.resume()
    }

func signup(email : String,password:String,uname:String, url : String, postCompleted : (succeeded: Bool, msg: String) -> ()) {
    var request = NSMutableURLRequest(URL: NSURL(string: url)!)
  
    var session = NSURLSession.sharedSession()
    request.HTTPMethod = "POST"
     let postParams = "User[username]=" + uname + "&User[password]=" + password + "&User[email]=" + email
    var err: NSError?
    request.HTTPBody = postParams.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
    
    var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
        println("Response: \(response)")
        var strData = NSString(data: data, encoding: NSUTF8StringEncoding)
        println("Body: \(strData)")
        var err: NSError?
        var json: AnyObject? = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil)
        
        var msg = "No message"
        var statusDict : NSDictionary = json?.valueForKey("status") as! NSDictionary
        
        var success = statusDict.valueForKey("status")!.integerValue!
        // self.error_code = NSString(format:"%d",success!) as String
        var errorMsg:String = statusDict.valueForKey("error") as! String
        if(err != nil) {
            println(err!.localizedDescription)
            let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
            println("Error could not parse JSON: '\(jsonStr)'")
            postCompleted(succeeded: false, msg: "Error")
        }
        else {
            if(success==1){
                var dictResult: NSDictionary = json?.valueForKey("result") as! NSDictionary
                var dictUser: NSDictionary = dictResult.valueForKey("user") as! NSDictionary
                var email: String = dictUser.valueForKey("email") as! String
                var uid: String = dictUser.valueForKey("id") as! String
                var uname: String = dictUser.valueForKey("username") as! String
                postCompleted(succeeded:true, msg: "Success")
            }else{
                postCompleted(succeeded:false, msg: errorMsg)
            }
            }
    })
    
    task.resume()
}
}
