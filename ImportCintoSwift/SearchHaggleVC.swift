//
//  SearchHaggleVC.swift
//  Haggles
//
//  Created by Aditi Manna on 03/08/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

import UIKit

class SearchHaggleVC: UIViewController {
    @IBAction func btnActionBack(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    @IBOutlet weak var tblActiveHaggle: UITableView!
    
    @IBOutlet weak var tblNearByHaggle: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let customTabBarVc = SSTabBarController(nibName: "SSTabBarController", bundle: nil)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let welcomeVC = storyboard.instantiateViewControllerWithIdentifier("MenuVC") as! MenuVC
        let WelcomeVcNavigation = UINavigationController(rootViewController:welcomeVC)
        WelcomeVcNavigation.navigationBar.hidden = true
        customTabBarVc.viewControllers = NSArray(object:WelcomeVcNavigation) as [AnyObject]
        self.view.addSubview(customTabBarVc.view)
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO("7.0"))
        {
            let view :UIView = UIView(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, 20))
            // view.backgroundColor = UIColor(red: 71/255, green: 154/255, blue: 65/255, alpha: 1)
            view.backgroundColor = UIColor(patternImage: UIImage(named: "green_patch.png")!)
            
            self.view.addSubview(view)
        }

        // Do any additional setup after loading the view.
    }
    func SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(version: NSString) -> Bool {
        return UIDevice.currentDevice().systemVersion.compare(version as String,
            options: NSStringCompareOptions.NumericSearch) != NSComparisonResult.OrderedAscending
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if(tableView.tag==100){
            return 2
        }else{
           return 5
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        if(tableView.tag == 100){
        var cell : HagglerCell = self.tblActiveHaggle.dequeueReusableCellWithIdentifier("Cell") as! HagglerCell
        return cell
        }else{
            var cell : HagglerCell = self.tblNearByHaggle.dequeueReusableCellWithIdentifier("Cell") as! HagglerCell
            return cell
        }
        
    }
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        self .performSegueWithIdentifier("segueHaggleDetails", sender: self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
