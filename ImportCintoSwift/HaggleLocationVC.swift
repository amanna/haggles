//
//  HaggleLocationVC.swift
//  Haggles
//
//  Created by Aditi Manna on 03/08/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

import UIKit
import CoreLocation
class HaggleLocationVC: UIViewController {
  //  let locationManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let customTabBarVc = SSTabBarController(nibName: "SSTabBarController", bundle: nil)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let welcomeVC = storyboard.instantiateViewControllerWithIdentifier("MenuVC") as! MenuVC
        let WelcomeVcNavigation = UINavigationController(rootViewController:welcomeVC)
        WelcomeVcNavigation.navigationBar.hidden = true
        customTabBarVc.viewControllers = NSArray(object:WelcomeVcNavigation) as [AnyObject]
        self.view.addSubview(customTabBarVc.view)
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO("7.0"))
        {
            let view :UIView = UIView(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, 20))
            // view.backgroundColor = UIColor(red: 71/255, green: 154/255, blue: 65/255, alpha: 1)
            view.backgroundColor = UIColor(patternImage: UIImage(named: "green_patch.png")!)
            
            self.view.addSubview(view)
        }

        
//        self.locationManager.delegate = self
//        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
//        self.locationManager.requestWhenInUseAuthorization()
//        self.locationManager.startUpdatingLocation()

        // Do any additional setup after loading the view.
    }
    func SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(version: NSString) -> Bool {
        return UIDevice.currentDevice().systemVersion.compare(version as String,
            options: NSStringCompareOptions.NumericSearch) != NSComparisonResult.OrderedAscending
    }
    @IBAction func btnBackAction(sender: UIButton) {
         self.navigationController?.popViewControllerAnimated(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!){
        CLGeocoder().reverseGeocodeLocation(manager.location, completionHandler: { (placemarks, error) -> Void in
            if (error != nil) {
                println("Error:" + error.localizedDescription)
                return
            }
            if placemarks.count > 0 {
                let pm = placemarks[0] as! CLPlacemark
                self.displayLocationInfo(pm)
            }else {
                println("Error with data")
                
            }
        })
    }
    func displayLocationInfo(placemark: CLPlacemark) {
       // self.locationManager.stopUpdatingLocation()
        println(placemark.locality)
        println(placemark.postalCode)
        println(placemark.administrativeArea)
        println(placemark.country)
        
    }
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        
        println("Error: " + error.localizedDescription)
        
    }
    
    @IBAction func btnSetMyCurrLocation(sender: UIButton) {
    }
    
    @IBAction func btnSetAnotherLoc(sender: UIButton) {
        //segueLocOther
        self.performSegueWithIdentifier("segueLocOther", sender: self)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
