//
//  JoinHaggle.swift
//  Haggles
//
//  Created by Aditi Manna on 18/08/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

import UIKit

class JoinHaggle: UIViewController {

    @IBOutlet weak var imgYellow: UIImageView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var btnClose: UIButton!
   
    @IBAction func btnCloseAction(sender: UIButton) {
        self.htConstraint.constant = 0;
        self.imgYellow .removeFromSuperview();
        self.btnClose.removeFromSuperview()
        
       
    }
    @IBOutlet weak var htConstraint: NSLayoutConstraint!
    @IBAction func btnEditHaggleAction(sender: UIButton) {
    }
    @IBAction func btnGroupChatAction(sender: UIButton) {
    }
    @IBAction func btnBackAction(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBOutlet weak var myScrollView: UIScrollView!
    @IBOutlet weak var viewuser: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let customTabBarVc = SSTabBarController(nibName: "SSTabBarController", bundle: nil)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let welcomeVC = storyboard.instantiateViewControllerWithIdentifier("MenuVC") as! MenuVC
        let WelcomeVcNavigation = UINavigationController(rootViewController:welcomeVC)
        WelcomeVcNavigation.navigationBar.hidden = true
        customTabBarVc.viewControllers = NSArray(object:WelcomeVcNavigation) as [AnyObject]
        self.view.addSubview(customTabBarVc.view)
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO("7.0"))
        {
            let view :UIView = UIView(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, 20))
            // view.backgroundColor = UIColor(red: 71/255, green: 154/255, blue: 65/255, alpha: 1)
            view.backgroundColor = UIColor(patternImage: UIImage(named: "green_patch.png")!)
            
            self.view.addSubview(view)
        }

        //add button in scrollview
        var xOrigin : CGFloat = 0.0
        for i in 0..<6
        {
           
            
            var button   = UIButton.buttonWithType(UIButtonType.System) as! UIButton
            button.frame = CGRectMake(xOrigin, (self.myScrollView.frame.size.height-54)/4, 42, 54)
            button.backgroundColor = UIColor.clearColor()
            
           // 32/32
            
            
            var imgUser = UIImageView(frame: CGRectMake(5, 0, 32 , 32))
            imgUser.image = UIImage(named: "Haggle-icon-1.png")
            button.addSubview(imgUser)
            
            
            var label = UILabel(frame: CGRectMake(0, 33, 42, 21))
            
            label.textAlignment = NSTextAlignment.Center
            label.text = "Gems"
            label.font = UIFont(name: label.font.fontName, size: 15)
            button.addSubview(label)
            
            
            self.myScrollView.addSubview(button)
            xOrigin = xOrigin + 10 + button.frame.size.width
            
        }
        self.myScrollView.contentSize = CGSize(width:xOrigin, height: self.myScrollView.frame.size.height)
        // Do any additional setup after loading the view.
    }
    func SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(version: NSString) -> Bool {
        return UIDevice.currentDevice().systemVersion.compare(version as String,
            options: NSStringCompareOptions.NumericSearch) != NSComparisonResult.OrderedAscending
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
