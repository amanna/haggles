//
//  LoginVC.swift
//  Haggles
//
//  Created by Aditi Manna on 04/08/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

import UIKit

class LoginVC: UIViewController,UITextFieldDelegate {
   
    @IBOutlet weak var txtPass: CTextField!
    @IBOutlet weak var txtEmail: CTextField!
     var  isJoinHaggle:NSString = ""
    var hud: MBProgressHUD = MBProgressHUD()
    @IBAction func btnSignUpAction(sender: UIButton) {
       // segueSignUp
        self.performSegueWithIdentifier("segueSignUp", sender: self)
    }
    @IBAction func btnForgotPassAction(sender: UIButton) {
    }
    @IBAction func btnLoginAction(sender: UIButton) {
        var hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        self.txtEmail .becomeFirstResponder()
        let global = GlobalConstants()
        let rest = RestAPISession()
        //call login webservice
        rest.login(self.txtEmail.text, password:self.txtPass.text, url:global.kBaseURLLogin) { (succeeded, msg) -> () in
            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
            
            if(succeeded){
                self.performSegueWithIdentifier("segueBuyProduct", sender: self)
//                if(self.isJoinHaggle == "join"){
//                    self.performSegueWithIdentifier("segueJoinHaggle", sender: self)
//                }else{
//                    self.performSegueWithIdentifier("segueWelcome", sender: self)
//                }
            }else{
                var refreshalert = UIAlertView(title:"Login", message:msg, delegate:nil, cancelButtonTitle: "OK")
                refreshalert.show()
                
            }
        }
        
    
    }
   
    @IBAction func btnBackAction(sender: UIButton) {
        if let navController = self.navigationController {
            navController.popViewControllerAnimated(true)
        }
    }
    @IBOutlet weak var myView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.myView.backgroundColor = UIColor(patternImage: UIImage(named: "sign-up&forget_blur.png")!)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        if(textField == self.txtEmail){
            self.txtPass .becomeFirstResponder()
            self.txtEmail.resignFirstResponder()
        }else if(textField == self.txtPass){
            self.txtPass.resignFirstResponder()
        }
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
