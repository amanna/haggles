//
//  GlobalConstants.swift
//  Haggles
//
//  Created by Aditi Manna on 19/08/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

import UIKit
import Foundation
class GlobalConstants: NSObject {
   
    let kBaseURLLogin : String = "http://dev1.businessprodemo.com/Haggles/php/index.php?r=api/Login"
    let kBaseURLSignUp : String = "http://dev1.businessprodemo.com/Haggles/php/index.php?r=api/Registration"
    
}
