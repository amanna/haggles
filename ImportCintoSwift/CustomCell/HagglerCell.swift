//
//  HagglerCell.swift
//  Haggles
//
//  Created by Aditi Manna on 30/07/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

import UIKit

class HagglerCell: UITableViewCell {

    @IBOutlet weak var lblProductNO: UILabel!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
