//
//  RateHaggleCell.swift
//  Haggles
//
//  Created by Aditi Manna on 07/09/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

import UIKit

class RateHaggleCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblDesc: UILabel!
  
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnDislike: UIButton!
      @IBOutlet weak var imgRate: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
