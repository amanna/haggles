//
//  ProductCell.swift
//  Haggles
//
//  Created by Aditi Manna on 07/08/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

import UIKit

class ProductCell: UICollectionViewCell {
    
    @IBOutlet weak var widthconstarint: NSLayoutConstraint!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblProdName: UILabel!
    @IBOutlet weak var htconstarint: NSLayoutConstraint!
    
    @IBOutlet weak var imgBack: UIImageView!
}
