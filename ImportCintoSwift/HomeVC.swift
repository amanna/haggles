//
//  HomeVC.swift
//  Haggles
//
//  Created by Aditi Manna on 14/08/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {
    @IBAction func btnExploreHaggle(sender: UIButton) {
        self.performSegueWithIdentifier("segueWelcome", sender: self)
    }

    @IBAction func btnFbLoginAction(sender: UIButton) {
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view?.backgroundColor = UIColor.blackColor()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func btnLoginAction(sender: UIButton) {
        self.performSegueWithIdentifier("segueLogin", sender: self)
    }
    @IBAction func btnSignUpAction(sender: UIButton) {
        self.performSegueWithIdentifier("segueSignUp", sender: self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
